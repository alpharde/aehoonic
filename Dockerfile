FROM alpine:3.17
RUN apk add gonic dumb-init
ADD run /usr/local/bin/run
RUN chmod +x /usr/local/bin/run
VOLUME /config
EXPOSE 4747
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["run"]
